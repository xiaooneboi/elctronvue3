## api

### 1.获得屏幕尺寸

https://blog.csdn.net/uotail/article/details/125173750
https://blog.csdn.net/weixin_42249565/article/details/127434702

获取屏幕
注意需要在 new 之后获取
```js
import { BrowserWindow, screen } from'electron';
const win = new BrowserWindow({
  width: screen.getPrimaryDisplay().workAreaSize.width, 
  height: screen.getPrimaryDisplay().workAreaSize.height, 
  webPreferences: {
    nodeIntegration: true
  }
});
```

### 2. 播放声音

https://www.jianshu.com/p/f5d2587cb743
https://www.cnblogs.com/Galesaur-wcy/p/15776060.html

### 3. 进程通讯

https://www.cnblogs.com/Galesaur-wcy/p/15776052.html

### 4. 图标

iconpark

https://iconpark.oceanengine.com/home

https://pidoutv.com/sites/9412.html

https://baijiahao.baidu.com/s?id=1688478890175871747&wfr=spider&for=pc

https://hao.archcookie.com/sites/1543.html

### 5.网址

https://cn.office-converter.com/svg


### 5. ctr+ship+p


### js方法

https://www.cnblogs.com/wenshaochang123/p/14830782.html


### ES6 方法

https://blog.csdn.net/swl979623074/article/details/78002341
ES6方法的简写a()｛｝等同于a:function(){}

```
const o = {
  method() {
    return "Hello!";
  }
};

// 等同于

const o = {
  method: function() {
    return "Hello!";
  }
};

```
