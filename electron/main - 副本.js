const { app, BrowserWindow ,Menu ,ipcMain } = require('electron')
const {join} = require('path')
const createWindow = () => {
    const win = new BrowserWindow({
        width: 800,
        height: 600
        //fullscreen: true, // 进入全屏模式
       // resizable: true,   // 允许调整窗口大小
        //frame: false, // 隐藏窗口的工具栏
    })
    // win.loadURL('http://localhost:5173')
    //win.loadFile('../dist/index.html')
const env = app.isPackaged ? 'production' : 'development'
const indexHtml = {
    development: 'http://localhost:5173', // 开发环境
    production: join(__dirname, '../dist/index.html') // 生产环境
}
win.loadURL(indexHtml[env])

 

// ipcMain.handle('check-password', async (event, password) => {  

//     const correctPassword = '123456'; // 替换为正确的密码  
//     return password === correctPassword;  
//   });


// Menu.setApplicationMenu(null);

let mainWindow=win;


  mainWindow.on('minimize', () => {
    console.log('窗口被最小化');
    // 在窗口最小化时执行相关操作
  });

  // 监听窗口最大化事件
  mainWindow.on('maximize', () => {
    console.log('窗口被最大化');
    // 在窗口最大化时执行相关操作
  });

  // 监听窗口恢复正常大小事件
  mainWindow.on('unmaximize', (event) => {
      // 阻止窗口关闭的默认行为
      event.preventDefault();
    console.log('窗口恢复正常大小');
    // 在窗口恢复正常大小时执行相关操作
  });

  mainWindow.on('close', function (e) {
    e.preventDefault();
  
    // 弹出确认对话框
    const choice = require('electron').dialog.showMessageBoxSync(win, {
      type: 'question',
      buttons: ['Yes', 'No'],
      title: 'Confirm',
      message: 'Are you sure you want to close the application?',
    });
  
    // 如果用户选择"Yes"，销毁窗口
    if (choice === 0) {
        mainWindow.destroy();
    }
  });
  

 

}



app.whenReady().then(() => {
    console.log('Electron 应用已启动');
    createWindow()
    app.on('activate', () => {
        if (BrowserWindow.getAllWindows().length === 0) createWindow()
    })
})
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') app.quit()
})


// 在这里监听 before-quit 事件  
app.on('before-quit', (event) => {  
    // 假设有一个函数 checkCondition 来判断是否可以关闭  
    const canClose = checkCondition();  
    
    if (!canClose) {  
      // 如果条件不满足，则取消关闭事件  
      event.preventDefault();  
      // 可以选择弹出警告或者进行其他操作  
      console.log('关闭程序的条件未满足，程序将保持打开状态。');  
      event.returnValue = false;
    } else {  
      // 如果条件满足，则允许程序关闭  
      console.log('关闭程序的条件满足，程序将关闭。');  
    }  
  });  

    
// 假设的 checkCondition 函数，你需要根据实际情况来实现这个函数  
function checkCondition() {  
    // 这里替换为你的实际条件判断逻辑  
    // 例如，检查某个文件是否存在，或者某个网络请求是否成功等  
    // 这里只是一个简单的示例，直接返回 true  
    return false;  
  }
