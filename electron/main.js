const { app, BrowserWindow ,Menu ,screen,ipcMain } = require('electron')
const {join, resolve} = require('path')
// const {width,height}=screen.getPrimaryDisplay().workAreaSize

const createWindow = () => {
    const win = new BrowserWindow({
        // width: 800,
        // height: 600,

        width: screen.getPrimaryDisplay().workAreaSize.width,
        height: screen.getPrimaryDisplay().workAreaSize.height,
        webPreferences:{
          preload:resolve(__dirname,'preload.js'),
          nodeIntegration:true
        }
    })
    // win.loadURL('http://localhost:5173')
    //win.loadFile('../dist/index.html')
const env = app.isPackaged ? 'production' : 'development'
const indexHtml = {
    development: 'http://localhost:5173', // 开发环境
    production: join(__dirname, '../dist/index.html') // 生产环境
}
win.loadURL(indexHtml[env])

// win.webContents.openDevTools();
}
app.whenReady().then(() => {
    console.log('Electron 111');
    createWindow()
    app.on('activate', () => {
        if (BrowserWindow.getAllWindows().length === 0) createWindow()
    })
})
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') app.quit()
})

ipcMain.on("saveFile",()=>{
    console.log("save1111 File ")
})