import { createApp } from 'vue'
import './style.css'
import App from './App.vue'

import ElementPlus from 'element-plus';  
import 'element-plus/theme-chalk/index.css';

// (function(doc, win) {
//     var docEl = doc.documentElement,
//         resizeEvt = "orientationchange" in window ? "orientationchange" : "resize",
//         recalc = function() {
//             if (docEl.clientWidth > 750) {
//                 docEl.style.fontSize = "100px";
//                 doc.getElementById("app").style.width = "100%";
//             } else {
//                 var width = docEl.clientWidth / 7.5;
//                 docEl.style.fontSize = width + "px";
//                 doc.getElementById("app").style.width = "100%";
//             }
//         };
//     if (!doc.addEventListener) return;
//     win.addEventListener(resizeEvt, recalc, false);
//     doc.addEventListener("DOMContentLoaded", recalc, false);
//   })(document, window);


  (function (doc, win) {
    var docEl = doc.documentElement
    var resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize'
    var recalc = function () {
      var clientWidth = docEl.clientWidth
      if (!clientWidth) return
      docEl.style.fontSize = 20 * (clientWidth / 1920) + 'px'
    }
    if (!doc.addEventListener) return
    win.addEventListener(resizeEvt, recalc, false)
    doc.addEventListener('DOMContentLoaded', recalc, false)
  })(document, window)

const app = createApp(App);  
app.use(ElementPlus);  
app.mount('#app');

console.log(window.api)
console.log(window.apis)
console.log(window)

// createApp(App).mount('#app')
