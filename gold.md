## 目标

### 1. 全屏显示


在 Electron 应用中实现全屏显示并自适应各种屏幕分辨率的功能通常是一个很常见的需求。你可以按照以下步骤来实现 Electron 应用的自适应全屏显示：

实现自适应全屏显示：
创建 Electron 窗口时设置全屏属性：
在创建 Electron 窗口时，可以设置 fullscreen: true 属性来让窗口进入全屏模式。同时，为了实现自适应全屏显示，还需要设置 resizable: true 以允许窗口大小调整。

```javascript
const { app, BrowserWindow } = require('electron');

function createWindow() {
  const mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    fullscreen: true, // 进入全屏模式
    resizable: true,   // 允许调整窗口大小
    webPreferences: {
      nodeIntegration: true
    }
  });

  // 加载应用的 index.html
  mainWindow.loadFile('index.html');
}

app.on('ready', createWindow);
```
监听窗口变化事件：
监听 resize 事件可以帮助你捕获窗口大小的变化，从而在窗口大小改变时更新应用界面以适应新的窗口大小。

```javascript
mainWindow.on('resize', () => {
  // 在窗口大小改变时，更新应用界面布局
});
```
处理窗口退出全屏：
如果用户想要退出全屏模式，可以监听键盘事件（如按下 Esc 键）或者提供一个退出全屏的按钮，然后通过调用 exitFullScreen() 方法来退出全屏模式。

```javascript
mainWindow.on('keydown', (event) => {
  if (event.key === 'Escape') {
    mainWindow.setFullScreen(false); // 退出全屏模式
  }
});
```
通过以上步骤，你可以实现在 Electron 应用中自适应全屏显示的功能。记得在开发过程中测试不同分辨率下的表现，以确保应用能够良好地适应各种屏幕。希望这些信息对你有所帮助！

### 2. 不显示工具栏



### 最大化 最小化

https://www.cnblogs.com/mica/p/10794751.html

https://www.jianshu.com/p/adf819473911

https://blog.csdn.net/fukaiit/article/details/91351448

https://www.jianshu.com/p/4ff966beda8b

https://blog.csdn.net/qq285679784/article/details/120432532



### 3.关闭事件

```
  mainWindow.on('minimize', () => {
    console.log('窗口被最小化');
    // 在窗口最小化时执行相关操作
  });

  // 监听窗口最大化事件
  mainWindow.on('maximize', () => {
    console.log('窗口被最大化');
    // 在窗口最大化时执行相关操作
  });

  // 监听窗口恢复正常大小事件
  mainWindow.on('unmaximize', () => {
    console.log('窗口恢复正常大小');
    // 在窗口恢复正常大小时执行相关操作
  });

  // 监听窗口关闭事件
  mainWindow.on('closed', () => {
    console.log('窗口被关闭');
    // 在窗口关闭时执行相关操作
    mainWindow = null;
  });

}
```

## 组织关闭

https://blog.csdn.net/qq_37834631/article/details/135077643


https://www.cnblogs.com/linjiangxian/p/16223681.html



自适应：
https://blog.csdn.net/qq_33951051/article/details/123051952
https://www.jb51.net/javascript/285849tcq.htm


## 脚手架

electron-egg
个入门简单、跨平台、企业级桌面软件开发框架
https://www.kaka996.com/

https://www.yuque.com/u34495/mivcfg

vueNextAdmin
后台开源免费模板
https://lyt-top.gitee.io/vue-next-admin-doc-preview/

https://gitee.com/wonderful-code/buildadmin#https://gitee.com/link?target=https%3A%2F%2Fdemo.buildadmin.com


## 安装包

https://www.jianshu.com/p/92b6db400933

https://www.jianshu.com/p/578fd696f638