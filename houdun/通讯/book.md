## 预加载脚本



```javascript
   const win = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences:{
          preload:resolve(__dirname,'preload.js'),
          nodeIntegration:true
        }
    })
```

preload： 设置预加载脚本的路径，

nodeIntegration:true 设置这个选项 才能在预加载脚本里面使用node 的api\



预加载脚本 当然 主进程和渲染进程之间的桥梁，在预加载脚本中可以使用部分的node api

和electron api，能在预加载脚本里面使用dom操作，但我们不建议这样做，dom操作还是尽量放在

渲染进程里面做。

```javascript
const fs=require("fs")
const content=fs.readFileSync("package.json","UTF-8");
console.log(content)

window.addEventListener("DOMContentLoaded",()=>{

    console.log(document.querySelector("div"))
})
```





为什么需要预加载脚本，因为直接在渲染进程和主进程之间通讯可能存在安全隐患，渲染进程可以直接操作文件操作



## 网络请求安全处理



![image-20240314113739647](assets/image-20240314113739647.png)

vscode 隐藏node_modules



控制请求web源的时候允许那些网址访问

```
<meta http-equiv="Content-Security-Policy" content="default-src 'self'; img-src https://*; child-src 'none';">

```





## 完成主进程和预加载脚本preload.js的通讯



### 1. 预加载脚本通知主进程执行操作

> 使用事件订阅的方式 send和on



main.js 

```javascript

ipcMain.on("saveFile",()=>{
    console.log("save File ")
})
```

> ipcMain.on("saveFile")



preload.js

```javascript
// ipcMain.send("saveFile")
ipcRenderer.send("saveFile")
```



> ipcRenderer.send("saveFile")





> 进程之间的通讯本质是函数调用，在函数中完成调用



### 2.  渲染进程和预加载脚本通讯



渲染进程和预加载脚本里面的变量和方式是完全隔离的，怎么通讯，、就需要使用

api把变量和方法完全暴露出去， 暴露出去的变量挂在在渲染进程的window全局对象上。

暴露出去的变量就全局共享了。





preload.js 里面的变量和函数如何暴露给渲染进程调用

桥接调用:



preload.js

```

contextBridge.exposeInMainWorld("apis",{})
```

渲染进程调用：

```javascript
console.log(window.apis)
console.log(window)
```



在渲染进程脚本renderer.js 触犯事件

![image-20240314131744793](assets/image-20240314131744793.png)

触发事件后，根据暴露的变量对象 调用预加载脚本里面的方法

![image-20240314131850930](assets/image-20240314131850930.png)

预加载脚本发送事件名称到主进程



在主进程中监听事件 执行逻辑

![image-20240314131950659](assets/image-20240314131950659.png)



主进程里面的逻辑就可以保存文档获取访问网络状态的访问。





### 3. 在预加载脚本中访问程序版本，然后DOM操作渲染进程显示结果



![image-20240314132501920](assets/image-20240314132501920.png)



![image-20240314132343358](assets/image-20240314132343358.png)



通过node 查看当前 进程运行环境的一些信息



preload.js 中使用一小部分开放给他的node 和electron api

renderer.js 可以使用我们前段的框架，纯js，不可以使用node 和electron api当然可以设置





![image-20240314133026814](assets/image-20240314133026814.png)

index.html



id="counter"

> 如何通过主进程改变这个值

通过主进程的单击菜单 控制渲染进程页面上面的数据+1

![image-20240314133212989](assets/image-20240314133212989.png)





![image-20240314133233031](assets/image-20240314133233031.png)



![image-20240314133305479](assets/image-20240314133305479.png)

![image-20240314133315807](assets/image-20240314133315807.png)



![image-20240314133341810](assets/image-20240314133341810.png)

传递渲染进程进去



![image-20240314133419146](assets/image-20240314133419146.png)



在单击里面找到 webContents 对象



>  我们要向渲染进程传递消息，我要找到我们要向那个渲染进程传递消息，（electron，一个窗口就是一个渲染进程，所以可以有多个）

> win.webContents



### 4. 主进程找到渲染进程，然后发送指令



主进程：主进程发送事件

![image-20240314133821671](assets/image-20240314133821671.png)





预加载脚本：预加载脚本监听事件

![image-20240314133908053](assets/image-20240314133908053.png)



预加载脚本里面操作DOM修改数量

![image-20240314134142167](assets/image-20240314134142167.png)

>  预加载脚本里面是可以操作DOM，但是我们不建议这样做，预加载脚本起的是承上启下的作用，操作DOM脚本建议还是放在渲染进程里面去做







主进程传参：

![image-20240314134327300](assets/image-20240314134327300.png)

预加载脚本接受参数

![image-20240314134403768](assets/image-20240314134403768.png)



![image-20240314134447018](assets/image-20240314134447018.png)

神奇

渲染进程DOM操作。

![image-20240314134708322](assets/image-20240314134708322.png)

神奇。



### 5.  双向通信的使用方式

触发按钮事件

renderer.js

![image-20240314143237695](assets/image-20240314143237695.png)

preload.js

![image-20240314143319035](assets/image-20240314143319035.png)

main.js

![image-20240314143401089](assets/image-20240314143401089.png)





---

返回消息

预加载脚本上设置监听事件

![image-20240314144154021](assets/image-20240314144154021.png)



主进程执行事件后，找到浏览器对象 发送事件

![image-20240314144408783](assets/image-20240314144408783.png)



找到我们的fromWebContents,找到我们的网页对象然后把消息发回去（还是事件通讯）



![image-20240314144634355](assets/image-20240314144634355.png)

> 预加载脚本的输出是在浏览器哪里输出



使用两者通讯，事件通讯



## 使用invoke进行进程双向通信



> 使用electron 提供的api来完成上传文件
>
>  页面上单击按钮，调用electron对的api然后再把结果返回到页面上来



> 渲染进程不能直接和主进程通讯

渲染进程调用：

![image-20240314145248933](assets/image-20240314145248933.png)





预加载脚本发送事件

![image-20240314145348991](assets/image-20240314145348991.png)

selectFile事件



主进程监听事件

不使用事件： 预加载脚本直接调用invoke

![image-20240314145509544](assets/image-20240314145509544.png)



主进程就不能用on而是用handle了

![image-20240314145600972](assets/image-20240314145600972.png)





![image-20240314145649623](assets/image-20240314145649623.png)





![image-20240314145748447](assets/image-20240314145748447.png)



如何返回数据



直接返回：

![image-20240314145816620](assets/image-20240314145816620.png)



![image-20240314145917983](assets/image-20240314145917983.png)



![image-20240314145959095](assets/image-20240314145959095.png)



回掉在渲染进程拿数据：渲染进程接收主进程结果

![image-20240314150115407](assets/image-20240314150115407.png)





![image-20240314150251359](assets/image-20240314150251359.png)





> 大量使用回掉函数，获得数据。







## 渲染进程向主进程传递参数



预加载脚本定义函数和事件

![image-20240314155231924](assets/image-20240314155231924.png)



主进程接受事件接受值

![image-20240314155308949](assets/image-20240314155308949.png)



渲染进程传递参数：

![image-20240314155401542](assets/image-20240314155401542.png)





找到网页对象 然后设置设置标题

![image-20240314155443258](assets/image-20240314155443258.png)



